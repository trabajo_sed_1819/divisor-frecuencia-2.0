----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.12.2018 17:53:44
-- Design Name: 
-- Module Name: sincronizador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_divider1 is
generic(
frec_in:  positive:=2;
frec_out:positive:=1
);
 Port(
 clk_in: in STD_LOGIC;
 reset: in STD_LOGIC;
 clk_out: out STD_LOGIC
 );
 end clk_divider1;
 
 architecture behavioral of clk_divider1 is
 signal clk_sig: STD_LOGIC;
 constant frec:integer:=frec_in/(frec_out*2);
 
 begin
  process(clk_in,reset)
   variable count:integer;
   begin
    if reset='1' then 
     count:=1;
     clk_sig<='0';
    elsif rising_edge(clk_in) then 
      if count>=frec then 
      count:=1;
      clk_sig<=not (clk_sig);
      else
       count:=count+1;
      end if;
     end if;
    end process;
    clk_out<=clk_sig;
    end behavioral; 
     